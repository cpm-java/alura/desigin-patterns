package aula.imposto;

public class TesteDeImpostosComplexos {
	public static void main(String[] args) {
		Imposto impostoMuitoAlto = new ImpostoMuitoAlto(new ICMS());

		Orcamento orcamento = new Orcamento(500);

		double valor = impostoMuitoAlto.calcula(orcamento);

		System.out.println(valor);
	}
}
