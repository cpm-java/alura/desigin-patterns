package aula.imposto;

public abstract class Imposto {

	protected Imposto outroImposto;

	public Imposto(Imposto imposto) {
		this.outroImposto = imposto;
	}

	public Imposto() {

	}

	protected abstract double calcula(Orcamento orcamento);

	protected double calculaDoOutroImposto(Orcamento orcamento) {
		if (outroImposto == null)
			return 0;

		return outroImposto.calcula(orcamento);
	}

}
