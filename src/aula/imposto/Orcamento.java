package aula.imposto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import aula.desconto.Item;

public class Orcamento {
	private double valor;
	private final List<Item> itens;

	public Orcamento(double valor) {
		this.valor = valor;
		this.itens = new ArrayList<>();
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public List<Item> getItens() {
		return Collections.unmodifiableList(itens);
	}

	public void addItens(Item item) {
		this.itens.add(item);
	}
}