package aula.imposto;

public class CalculadorDeImpostos {

	public void realizaCalculo(Orcamento orcamento, Imposto imposto) {
		double icms = imposto.calcula(orcamento);
		System.out.println(icms);
	}

	public void realizaCalculoICMS(Orcamento orcamento, Imposto imposto) {
		double icms = new ICMS().calcula(orcamento);
		System.out.println(icms);
	}

	public void realizaCalculoISS(Orcamento orcamento, Imposto imposto) {
		double icms = new ICMS().calcula(orcamento);
		System.out.println(icms);
	}
}