package aula.imposto;

import java.util.List;

import aula.desconto.Item;

public class IHIT extends TemplateDeImpostoCondicional {

	@Override
	protected double minimaTaxacao(Orcamento orcamento) {
		return (orcamento.getValor() * 0.13) + 100;
	}

	@Override
	protected double maximaTaxacao(Orcamento orcamento) {
		return (orcamento.getValor() * 0.01) + orcamento.getItens().size();
	}

	@Override
	protected boolean deveUsarMaximaTaxacao(Orcamento orcamento) {
		List<Item> itens = orcamento.getItens();
		for (int i = 0; i < itens.size(); i++) {
			for (int j = i; j < itens.size(); j++) {
				if (itens.get(i).getNome().equalsIgnoreCase(itens.get(j).getNome()))
					return true;
			}
		}

		return false;
	}
}