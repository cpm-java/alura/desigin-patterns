package aula.desconto;

import aula.imposto.Orcamento;

public class DescontoPorMaisDeQuinhentoReais implements Desconto {
	private Desconto proximo;

	public double desconta(Orcamento orcamento) {
		if (orcamento.getValor() > 500) {
			return orcamento.getValor() * 0.07;
		}

		return proximo.desconta(orcamento);
	}

	@Override
	public void setProximo(Desconto proximo) {
		this.proximo = proximo;
	}
}
