package aula.desconto;

import aula.imposto.Orcamento;

public interface Desconto {
	double desconta(Orcamento orcamento);

	public void setProximo(Desconto proximo);
}
