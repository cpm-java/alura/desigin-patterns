package aula.investimento;

public interface Investimento {
	double calcula(Conta conta);
}