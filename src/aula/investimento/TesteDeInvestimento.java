package aula.investimento;

public class TesteDeInvestimento {
	public static void main(String[] args) {
		Investimento cons = new Conservador();
		Investimento mode = new Moderado();
		Investimento arro = new Arrojado();

		Conta orcamento = new Conta();
		orcamento.deposita(100.0);

		RealizadorDeInvestimentos realizador = new RealizadorDeInvestimentos();

		realizador.realiza(orcamento, cons);
		realizador.realiza(orcamento, mode);
		realizador.realiza(orcamento, arro);
	}
}
