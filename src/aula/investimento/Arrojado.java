package aula.investimento;

public class Arrojado implements Investimento {

	@Override
	public double calcula(Conta conta) {
		int chute = new java.util.Random().nextInt(100);
		if (chute <= 20) {
			return conta.getSaldo() * 0.05;
		} else if (chute <= 50) {
			return conta.getSaldo() * 0.025;
		} else {
			return conta.getSaldo() * 0.006;
		}
	}
}
